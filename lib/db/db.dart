import 'package:orm/logger.dart';

import '../src/generated/prisma/prisma_client.dart';

final postgres = PrismaClient(
  stdout: Event.values, // print all events to the console
  datasources: Datasources(
    db: 'postgresql://postgres:postgres@127.0.0.1:5432/db?schema=public',
  ),
);