import 'package:dart_demo/client/dto.dart';
import 'package:dart_demo/src/generated/prisma/prisma_client.dart';

import '../types.dart';

Future<DogFactEntity> save(DogFactDto dogFact, SAVE save) async {
  return await save(data: DogFactEntityCreateInput(
      factId: dogFact.data[0].id,
      fact: dogFact.data[0].attributes.body));
}