import 'package:dart_demo/src/generated/prisma/prisma_client.dart';
import 'package:http/http.dart';
import 'package:orm/graphql.dart';

import 'client/dto.dart';

typedef GET = Future<Response> Function(Uri url,
    {Map<String, String>? headers});

typedef SAVE = DogFactEntityFluent<DogFactEntity> Function(
    {required DogFactEntityCreateInput data});

typedef QUERY = Future<Object?> Function(Iterable<GraphQLField>);

typedef FACT = Future<DogFactDto> Function(GET get);
