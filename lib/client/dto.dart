import 'dart:convert';

class DogFactDto{
  late List<FactData> data;

  DogFactDto({required this.data});

  DogFactDto.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <FactData>[];
      json['data'].forEach((v) {
        data.add(new FactData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data.map((v) => v.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'DogFact{ data: ${jsonEncode(data)} }';
  }

}

class FactData {
  late String id;
  late String type;
  late FactAttribute attributes;

  FactData({required this.id, required this.type, required this.attributes});

  FactData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    attributes = new FactAttribute.fromJson(json['attributes']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['attributes'] = this.attributes.toJson();
    return data;
  }
}

class FactAttribute {
  late String body;

  FactAttribute({required this.body});

  FactAttribute.fromJson(Map<String, dynamic> json) {
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['body'] = this.body;
    return data;
  }
}
