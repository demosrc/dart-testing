import 'package:dart_demo/client/dto.dart';
import 'package:dart_demo/types.dart';
import 'package:dart_demo/util.dart';

import 'base_url.dart';

Future<DogFactDto> fact(GET get) {
  return get(
      url(API_PATH)
  ).then(body);
}