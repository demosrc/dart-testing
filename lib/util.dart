import 'dart:convert';

import 'package:dart_demo/client/dto.dart';
import 'package:http/http.dart';

import 'client/base_url.dart';

DogFactDto body(Response response) {
  return DogFactDto.fromJson(jsonDecode(response.body));
}

Uri url(String apiPath){
  return Uri.https(BASE_URL, apiPath);
}