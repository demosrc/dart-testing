// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:json_annotation/json_annotation.dart';
import 'package:orm/binary_engine.dart' as _i5;
import 'package:orm/engine_core.dart' as _i3;
import 'package:orm/graphql.dart' as _i2;
import 'package:orm/logger.dart' as _i4;
import 'package:orm/orm.dart' as _i1;
import 'package:orm/orm.dart' show DateTimeJsonConverter;

part 'prisma_client.g.dart';

enum DogFactEntityScalarFieldEnum implements _i1.PrismaEnum {
  id,
  factId,
  fact;

  @override
  String? get originalName => null;
}

enum SortOrder implements _i1.PrismaEnum {
  asc,
  desc;

  @override
  String? get originalName => null;
}

enum QueryMode implements _i1.PrismaEnum {
  @JsonValue('default')
  $default(r'default'),
  insensitive;

  const QueryMode([this.originalName]);

  @override
  final String? originalName;
}

@_i1.jsonSerializable
class DogFactEntityWhereInput implements _i1.JsonSerializable {
  const DogFactEntityWhereInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityWhereInput.fromJson(Map<String, dynamic> json) =>
      _$DogFactEntityWhereInputFromJson(json);

  final Iterable<DogFactEntityWhereInput>? AND;

  final Iterable<DogFactEntityWhereInput>? OR;

  final Iterable<DogFactEntityWhereInput>? NOT;

  final IntFilter? id;

  final StringFilter? factId;

  final StringFilter? fact;

  @override
  Map<String, dynamic> toJson() => _$DogFactEntityWhereInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityOrderByWithRelationInput implements _i1.JsonSerializable {
  const DogFactEntityOrderByWithRelationInput({
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityOrderByWithRelationInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityOrderByWithRelationInputFromJson(json);

  final SortOrder? id;

  final SortOrder? factId;

  final SortOrder? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityOrderByWithRelationInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityWhereUniqueInput implements _i1.JsonSerializable {
  const DogFactEntityWhereUniqueInput({
    this.id,
    this.factId,
  });

  factory DogFactEntityWhereUniqueInput.fromJson(Map<String, dynamic> json) =>
      _$DogFactEntityWhereUniqueInputFromJson(json);

  final int? id;

  final String? factId;

  @override
  Map<String, dynamic> toJson() => _$DogFactEntityWhereUniqueInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityOrderByWithAggregationInput implements _i1.JsonSerializable {
  const DogFactEntityOrderByWithAggregationInput({
    this.id,
    this.factId,
    this.fact,
    this.$count,
    this.$avg,
    this.$max,
    this.$min,
    this.$sum,
  });

  factory DogFactEntityOrderByWithAggregationInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityOrderByWithAggregationInputFromJson(json);

  final SortOrder? id;

  final SortOrder? factId;

  final SortOrder? fact;

  @JsonKey(name: r'_count')
  final DogFactEntityCountOrderByAggregateInput? $count;

  @JsonKey(name: r'_avg')
  final DogFactEntityAvgOrderByAggregateInput? $avg;

  @JsonKey(name: r'_max')
  final DogFactEntityMaxOrderByAggregateInput? $max;

  @JsonKey(name: r'_min')
  final DogFactEntityMinOrderByAggregateInput? $min;

  @JsonKey(name: r'_sum')
  final DogFactEntitySumOrderByAggregateInput? $sum;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityOrderByWithAggregationInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityScalarWhereWithAggregatesInput
    implements _i1.JsonSerializable {
  const DogFactEntityScalarWhereWithAggregatesInput({
    this.AND,
    this.OR,
    this.NOT,
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityScalarWhereWithAggregatesInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityScalarWhereWithAggregatesInputFromJson(json);

  final Iterable<DogFactEntityScalarWhereWithAggregatesInput>? AND;

  final Iterable<DogFactEntityScalarWhereWithAggregatesInput>? OR;

  final Iterable<DogFactEntityScalarWhereWithAggregatesInput>? NOT;

  final IntWithAggregatesFilter? id;

  final StringWithAggregatesFilter? factId;

  final StringWithAggregatesFilter? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityScalarWhereWithAggregatesInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityCreateInput implements _i1.JsonSerializable {
  const DogFactEntityCreateInput({
    required this.factId,
    required this.fact,
  });

  factory DogFactEntityCreateInput.fromJson(Map<String, dynamic> json) =>
      _$DogFactEntityCreateInputFromJson(json);

  final String factId;

  final String fact;

  @override
  Map<String, dynamic> toJson() => _$DogFactEntityCreateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityUncheckedCreateInput implements _i1.JsonSerializable {
  const DogFactEntityUncheckedCreateInput({
    this.id,
    required this.factId,
    required this.fact,
  });

  factory DogFactEntityUncheckedCreateInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityUncheckedCreateInputFromJson(json);

  final int? id;

  final String factId;

  final String fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityUncheckedCreateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityUpdateInput implements _i1.JsonSerializable {
  const DogFactEntityUpdateInput({
    this.factId,
    this.fact,
  });

  factory DogFactEntityUpdateInput.fromJson(Map<String, dynamic> json) =>
      _$DogFactEntityUpdateInputFromJson(json);

  final StringFieldUpdateOperationsInput? factId;

  final StringFieldUpdateOperationsInput? fact;

  @override
  Map<String, dynamic> toJson() => _$DogFactEntityUpdateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityUncheckedUpdateInput implements _i1.JsonSerializable {
  const DogFactEntityUncheckedUpdateInput({
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityUncheckedUpdateInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityUncheckedUpdateInputFromJson(json);

  final IntFieldUpdateOperationsInput? id;

  final StringFieldUpdateOperationsInput? factId;

  final StringFieldUpdateOperationsInput? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityUncheckedUpdateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityCreateManyInput implements _i1.JsonSerializable {
  const DogFactEntityCreateManyInput({
    this.id,
    required this.factId,
    required this.fact,
  });

  factory DogFactEntityCreateManyInput.fromJson(Map<String, dynamic> json) =>
      _$DogFactEntityCreateManyInputFromJson(json);

  final int? id;

  final String factId;

  final String fact;

  @override
  Map<String, dynamic> toJson() => _$DogFactEntityCreateManyInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityUpdateManyMutationInput implements _i1.JsonSerializable {
  const DogFactEntityUpdateManyMutationInput({
    this.factId,
    this.fact,
  });

  factory DogFactEntityUpdateManyMutationInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityUpdateManyMutationInputFromJson(json);

  final StringFieldUpdateOperationsInput? factId;

  final StringFieldUpdateOperationsInput? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityUpdateManyMutationInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityUncheckedUpdateManyInput implements _i1.JsonSerializable {
  const DogFactEntityUncheckedUpdateManyInput({
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityUncheckedUpdateManyInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityUncheckedUpdateManyInputFromJson(json);

  final IntFieldUpdateOperationsInput? id;

  final StringFieldUpdateOperationsInput? factId;

  final StringFieldUpdateOperationsInput? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityUncheckedUpdateManyInputToJson(this);
}

@_i1.jsonSerializable
class IntFilter implements _i1.JsonSerializable {
  const IntFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  factory IntFilter.fromJson(Map<String, dynamic> json) =>
      _$IntFilterFromJson(json);

  final int? equals;

  @JsonKey(name: r'in')
  final Iterable<int>? $in;

  final Iterable<int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final NestedIntFilter? not;

  @override
  Map<String, dynamic> toJson() => _$IntFilterToJson(this);
}

@_i1.jsonSerializable
class StringFilter implements _i1.JsonSerializable {
  const StringFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.mode,
    this.not,
  });

  factory StringFilter.fromJson(Map<String, dynamic> json) =>
      _$StringFilterFromJson(json);

  final String? equals;

  @JsonKey(name: r'in')
  final Iterable<String>? $in;

  final Iterable<String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final QueryMode? mode;

  final NestedStringFilter? not;

  @override
  Map<String, dynamic> toJson() => _$StringFilterToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityCountOrderByAggregateInput implements _i1.JsonSerializable {
  const DogFactEntityCountOrderByAggregateInput({
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityCountOrderByAggregateInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityCountOrderByAggregateInputFromJson(json);

  final SortOrder? id;

  final SortOrder? factId;

  final SortOrder? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityCountOrderByAggregateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityAvgOrderByAggregateInput implements _i1.JsonSerializable {
  const DogFactEntityAvgOrderByAggregateInput({this.id});

  factory DogFactEntityAvgOrderByAggregateInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityAvgOrderByAggregateInputFromJson(json);

  final SortOrder? id;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityAvgOrderByAggregateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityMaxOrderByAggregateInput implements _i1.JsonSerializable {
  const DogFactEntityMaxOrderByAggregateInput({
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityMaxOrderByAggregateInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityMaxOrderByAggregateInputFromJson(json);

  final SortOrder? id;

  final SortOrder? factId;

  final SortOrder? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityMaxOrderByAggregateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntityMinOrderByAggregateInput implements _i1.JsonSerializable {
  const DogFactEntityMinOrderByAggregateInput({
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityMinOrderByAggregateInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntityMinOrderByAggregateInputFromJson(json);

  final SortOrder? id;

  final SortOrder? factId;

  final SortOrder? fact;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntityMinOrderByAggregateInputToJson(this);
}

@_i1.jsonSerializable
class DogFactEntitySumOrderByAggregateInput implements _i1.JsonSerializable {
  const DogFactEntitySumOrderByAggregateInput({this.id});

  factory DogFactEntitySumOrderByAggregateInput.fromJson(
          Map<String, dynamic> json) =>
      _$DogFactEntitySumOrderByAggregateInputFromJson(json);

  final SortOrder? id;

  @override
  Map<String, dynamic> toJson() =>
      _$DogFactEntitySumOrderByAggregateInputToJson(this);
}

@_i1.jsonSerializable
class IntWithAggregatesFilter implements _i1.JsonSerializable {
  const IntWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory IntWithAggregatesFilter.fromJson(Map<String, dynamic> json) =>
      _$IntWithAggregatesFilterFromJson(json);

  final int? equals;

  @JsonKey(name: r'in')
  final Iterable<int>? $in;

  final Iterable<int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final NestedIntWithAggregatesFilter? not;

  @JsonKey(name: r'_count')
  final NestedIntFilter? $count;

  @JsonKey(name: r'_avg')
  final NestedFloatFilter? $avg;

  @JsonKey(name: r'_sum')
  final NestedIntFilter? $sum;

  @JsonKey(name: r'_min')
  final NestedIntFilter? $min;

  @JsonKey(name: r'_max')
  final NestedIntFilter? $max;

  @override
  Map<String, dynamic> toJson() => _$IntWithAggregatesFilterToJson(this);
}

@_i1.jsonSerializable
class StringWithAggregatesFilter implements _i1.JsonSerializable {
  const StringWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.mode,
    this.not,
    this.$count,
    this.$min,
    this.$max,
  });

  factory StringWithAggregatesFilter.fromJson(Map<String, dynamic> json) =>
      _$StringWithAggregatesFilterFromJson(json);

  final String? equals;

  @JsonKey(name: r'in')
  final Iterable<String>? $in;

  final Iterable<String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final QueryMode? mode;

  final NestedStringWithAggregatesFilter? not;

  @JsonKey(name: r'_count')
  final NestedIntFilter? $count;

  @JsonKey(name: r'_min')
  final NestedStringFilter? $min;

  @JsonKey(name: r'_max')
  final NestedStringFilter? $max;

  @override
  Map<String, dynamic> toJson() => _$StringWithAggregatesFilterToJson(this);
}

@_i1.jsonSerializable
class StringFieldUpdateOperationsInput implements _i1.JsonSerializable {
  const StringFieldUpdateOperationsInput({this.set});

  factory StringFieldUpdateOperationsInput.fromJson(
          Map<String, dynamic> json) =>
      _$StringFieldUpdateOperationsInputFromJson(json);

  final String? set;

  @override
  Map<String, dynamic> toJson() =>
      _$StringFieldUpdateOperationsInputToJson(this);
}

@_i1.jsonSerializable
class IntFieldUpdateOperationsInput implements _i1.JsonSerializable {
  const IntFieldUpdateOperationsInput({
    this.set,
    this.increment,
    this.decrement,
    this.multiply,
    this.divide,
  });

  factory IntFieldUpdateOperationsInput.fromJson(Map<String, dynamic> json) =>
      _$IntFieldUpdateOperationsInputFromJson(json);

  final int? set;

  final int? increment;

  final int? decrement;

  final int? multiply;

  final int? divide;

  @override
  Map<String, dynamic> toJson() => _$IntFieldUpdateOperationsInputToJson(this);
}

@_i1.jsonSerializable
class NestedIntFilter implements _i1.JsonSerializable {
  const NestedIntFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  factory NestedIntFilter.fromJson(Map<String, dynamic> json) =>
      _$NestedIntFilterFromJson(json);

  final int? equals;

  @JsonKey(name: r'in')
  final Iterable<int>? $in;

  final Iterable<int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final NestedIntFilter? not;

  @override
  Map<String, dynamic> toJson() => _$NestedIntFilterToJson(this);
}

@_i1.jsonSerializable
class NestedStringFilter implements _i1.JsonSerializable {
  const NestedStringFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.not,
  });

  factory NestedStringFilter.fromJson(Map<String, dynamic> json) =>
      _$NestedStringFilterFromJson(json);

  final String? equals;

  @JsonKey(name: r'in')
  final Iterable<String>? $in;

  final Iterable<String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final NestedStringFilter? not;

  @override
  Map<String, dynamic> toJson() => _$NestedStringFilterToJson(this);
}

@_i1.jsonSerializable
class NestedIntWithAggregatesFilter implements _i1.JsonSerializable {
  const NestedIntWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
    this.$count,
    this.$avg,
    this.$sum,
    this.$min,
    this.$max,
  });

  factory NestedIntWithAggregatesFilter.fromJson(Map<String, dynamic> json) =>
      _$NestedIntWithAggregatesFilterFromJson(json);

  final int? equals;

  @JsonKey(name: r'in')
  final Iterable<int>? $in;

  final Iterable<int>? notIn;

  final int? lt;

  final int? lte;

  final int? gt;

  final int? gte;

  final NestedIntWithAggregatesFilter? not;

  @JsonKey(name: r'_count')
  final NestedIntFilter? $count;

  @JsonKey(name: r'_avg')
  final NestedFloatFilter? $avg;

  @JsonKey(name: r'_sum')
  final NestedIntFilter? $sum;

  @JsonKey(name: r'_min')
  final NestedIntFilter? $min;

  @JsonKey(name: r'_max')
  final NestedIntFilter? $max;

  @override
  Map<String, dynamic> toJson() => _$NestedIntWithAggregatesFilterToJson(this);
}

@_i1.jsonSerializable
class NestedFloatFilter implements _i1.JsonSerializable {
  const NestedFloatFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.not,
  });

  factory NestedFloatFilter.fromJson(Map<String, dynamic> json) =>
      _$NestedFloatFilterFromJson(json);

  final double? equals;

  @JsonKey(name: r'in')
  final Iterable<double>? $in;

  final Iterable<double>? notIn;

  final double? lt;

  final double? lte;

  final double? gt;

  final double? gte;

  final NestedFloatFilter? not;

  @override
  Map<String, dynamic> toJson() => _$NestedFloatFilterToJson(this);
}

@_i1.jsonSerializable
class NestedStringWithAggregatesFilter implements _i1.JsonSerializable {
  const NestedStringWithAggregatesFilter({
    this.equals,
    this.$in,
    this.notIn,
    this.lt,
    this.lte,
    this.gt,
    this.gte,
    this.contains,
    this.startsWith,
    this.endsWith,
    this.not,
    this.$count,
    this.$min,
    this.$max,
  });

  factory NestedStringWithAggregatesFilter.fromJson(
          Map<String, dynamic> json) =>
      _$NestedStringWithAggregatesFilterFromJson(json);

  final String? equals;

  @JsonKey(name: r'in')
  final Iterable<String>? $in;

  final Iterable<String>? notIn;

  final String? lt;

  final String? lte;

  final String? gt;

  final String? gte;

  final String? contains;

  final String? startsWith;

  final String? endsWith;

  final NestedStringWithAggregatesFilter? not;

  @JsonKey(name: r'_count')
  final NestedIntFilter? $count;

  @JsonKey(name: r'_min')
  final NestedStringFilter? $min;

  @JsonKey(name: r'_max')
  final NestedStringFilter? $max;

  @override
  Map<String, dynamic> toJson() =>
      _$NestedStringWithAggregatesFilterToJson(this);
}

@_i1.jsonSerializable
class DogFactEntity implements _i1.JsonSerializable {
  const DogFactEntity({
    required this.id,
    required this.factId,
    required this.fact,
  });

  factory DogFactEntity.fromJson(Map<String, dynamic> json) =>
      _$DogFactEntityFromJson(json);

  final int id;

  final String factId;

  final String fact;

  @override
  Map<String, dynamic> toJson() => _$DogFactEntityToJson(this);
}

class DogFactEntityFluent<T> extends _i1.PrismaFluent<T> {
  const DogFactEntityFluent(
    super.original,
    super.$query,
  );
}

extension DogFactEntityModelDelegateExtension
    on _i1.ModelDelegate<DogFactEntity> {
  DogFactEntityFluent<DogFactEntity?> findUnique(
      {required DogFactEntityWhereUniqueInput where}) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      )
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'findUniqueDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'findUniqueDogFactEntity',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : null);
    return DogFactEntityFluent<DogFactEntity?>(
      future,
      query,
    );
  }

  DogFactEntityFluent<DogFactEntity> findUniqueOrThrow(
      {required DogFactEntityWhereUniqueInput where}) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      )
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'findUniqueDogFactEntityOrThrow',
          fields: fields,
          args: args,
        )
      ]),
      key: r'findUniqueDogFactEntityOrThrow',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : throw Exception(
                'Not found OutputTypeRefType.string(value: DogFactEntity)'));
    return DogFactEntityFluent<DogFactEntity>(
      future,
      query,
    );
  }

  DogFactEntityFluent<DogFactEntity?> findFirst({
    DogFactEntityWhereInput? where,
    Iterable<DogFactEntityOrderByWithRelationInput>? orderBy,
    DogFactEntityWhereUniqueInput? cursor,
    int? take,
    int? skip,
    Iterable<DogFactEntityScalarFieldEnum>? distinct,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      ),
      _i2.GraphQLArg(
        r'orderBy',
        orderBy,
      ),
      _i2.GraphQLArg(
        r'cursor',
        cursor,
      ),
      _i2.GraphQLArg(
        r'take',
        take,
      ),
      _i2.GraphQLArg(
        r'skip',
        skip,
      ),
      _i2.GraphQLArg(
        r'distinct',
        distinct,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'findFirstDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'findFirstDogFactEntity',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : null);
    return DogFactEntityFluent<DogFactEntity?>(
      future,
      query,
    );
  }

  DogFactEntityFluent<DogFactEntity> findFirstOrThrow({
    DogFactEntityWhereInput? where,
    Iterable<DogFactEntityOrderByWithRelationInput>? orderBy,
    DogFactEntityWhereUniqueInput? cursor,
    int? take,
    int? skip,
    Iterable<DogFactEntityScalarFieldEnum>? distinct,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      ),
      _i2.GraphQLArg(
        r'orderBy',
        orderBy,
      ),
      _i2.GraphQLArg(
        r'cursor',
        cursor,
      ),
      _i2.GraphQLArg(
        r'take',
        take,
      ),
      _i2.GraphQLArg(
        r'skip',
        skip,
      ),
      _i2.GraphQLArg(
        r'distinct',
        distinct,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'findFirstDogFactEntityOrThrow',
          fields: fields,
          args: args,
        )
      ]),
      key: r'findFirstDogFactEntityOrThrow',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : throw Exception(
                'Not found OutputTypeRefType.string(value: DogFactEntity)'));
    return DogFactEntityFluent<DogFactEntity>(
      future,
      query,
    );
  }

  Future<Iterable<DogFactEntity>> findMany({
    DogFactEntityWhereInput? where,
    Iterable<DogFactEntityOrderByWithRelationInput>? orderBy,
    DogFactEntityWhereUniqueInput? cursor,
    int? take,
    int? skip,
    Iterable<DogFactEntityScalarFieldEnum>? distinct,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      ),
      _i2.GraphQLArg(
        r'orderBy',
        orderBy,
      ),
      _i2.GraphQLArg(
        r'cursor',
        cursor,
      ),
      _i2.GraphQLArg(
        r'take',
        take,
      ),
      _i2.GraphQLArg(
        r'skip',
        skip,
      ),
      _i2.GraphQLArg(
        r'distinct',
        distinct,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'findManyDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'findManyDogFactEntity',
    );
    final fields = DogFactEntityScalarFieldEnum.values.toGraphQLFields();
    compiler(Iterable<Map> findManyDogFactEntity) =>
        findManyDogFactEntity.map((Map findManyDogFactEntity) =>
            DogFactEntity.fromJson(findManyDogFactEntity.cast()));
    return query(fields).then((json) => json is Iterable
        ? compiler(json.cast())
        : throw Exception('Unable to parse response'));
  }

  DogFactEntityFluent<DogFactEntity> create(
      {required DogFactEntityCreateInput data}) {
    final args = [
      _i2.GraphQLArg(
        r'data',
        data,
      )
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $mutation([
        _i2.GraphQLField(
          r'createOneDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'createOneDogFactEntity',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : throw Exception(
                'Not found OutputTypeRefType.string(value: DogFactEntity)'));
    return DogFactEntityFluent<DogFactEntity>(
      future,
      query,
    );
  }

  Future<AffectedRowsOutput> createMany({
    required Iterable<DogFactEntityCreateManyInput> data,
    bool? skipDuplicates,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'data',
        data,
      ),
      _i2.GraphQLArg(
        r'skipDuplicates',
        skipDuplicates,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $mutation([
        _i2.GraphQLField(
          r'createManyDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'createManyDogFactEntity',
    );
    final fields = const ['count'].map((e) => _i2.GraphQLField(e));
    compiler(Map createManyDogFactEntity) =>
        AffectedRowsOutput.fromJson(createManyDogFactEntity.cast());
    return query(fields).then((json) => json is Map
        ? compiler(json)
        : throw Exception('Unable to parse response'));
  }

  DogFactEntityFluent<DogFactEntity?> update({
    required DogFactEntityUpdateInput data,
    required DogFactEntityWhereUniqueInput where,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'data',
        data,
      ),
      _i2.GraphQLArg(
        r'where',
        where,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $mutation([
        _i2.GraphQLField(
          r'updateOneDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'updateOneDogFactEntity',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : null);
    return DogFactEntityFluent<DogFactEntity?>(
      future,
      query,
    );
  }

  Future<AffectedRowsOutput> updateMany({
    required DogFactEntityUpdateManyMutationInput data,
    DogFactEntityWhereInput? where,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'data',
        data,
      ),
      _i2.GraphQLArg(
        r'where',
        where,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $mutation([
        _i2.GraphQLField(
          r'updateManyDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'updateManyDogFactEntity',
    );
    final fields = const ['count'].map((e) => _i2.GraphQLField(e));
    compiler(Map updateManyDogFactEntity) =>
        AffectedRowsOutput.fromJson(updateManyDogFactEntity.cast());
    return query(fields).then((json) => json is Map
        ? compiler(json)
        : throw Exception('Unable to parse response'));
  }

  DogFactEntityFluent<DogFactEntity> upsert({
    required DogFactEntityWhereUniqueInput where,
    required DogFactEntityCreateInput create,
    required DogFactEntityUpdateInput update,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      ),
      _i2.GraphQLArg(
        r'create',
        create,
      ),
      _i2.GraphQLArg(
        r'update',
        update,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $mutation([
        _i2.GraphQLField(
          r'upsertOneDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'upsertOneDogFactEntity',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : throw Exception(
                'Not found OutputTypeRefType.string(value: DogFactEntity)'));
    return DogFactEntityFluent<DogFactEntity>(
      future,
      query,
    );
  }

  DogFactEntityFluent<DogFactEntity?> delete(
      {required DogFactEntityWhereUniqueInput where}) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      )
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $mutation([
        _i2.GraphQLField(
          r'deleteOneDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'deleteOneDogFactEntity',
    );
    final future = query(DogFactEntityScalarFieldEnum.values.toGraphQLFields())
        .then((json) => json is Map
            ? DogFactEntity.fromJson(json.cast<String, dynamic>())
            : null);
    return DogFactEntityFluent<DogFactEntity?>(
      future,
      query,
    );
  }

  Future<AffectedRowsOutput> deleteMany({DogFactEntityWhereInput? where}) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      )
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $mutation([
        _i2.GraphQLField(
          r'deleteManyDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'deleteManyDogFactEntity',
    );
    final fields = const ['count'].map((e) => _i2.GraphQLField(e));
    compiler(Map deleteManyDogFactEntity) =>
        AffectedRowsOutput.fromJson(deleteManyDogFactEntity.cast());
    return query(fields).then((json) => json is Map
        ? compiler(json)
        : throw Exception('Unable to parse response'));
  }

  AggregateDogFactEntity aggregate({
    DogFactEntityWhereInput? where,
    Iterable<DogFactEntityOrderByWithRelationInput>? orderBy,
    DogFactEntityWhereUniqueInput? cursor,
    int? take,
    int? skip,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      ),
      _i2.GraphQLArg(
        r'orderBy',
        orderBy,
      ),
      _i2.GraphQLArg(
        r'cursor',
        cursor,
      ),
      _i2.GraphQLArg(
        r'take',
        take,
      ),
      _i2.GraphQLArg(
        r'skip',
        skip,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'aggregateDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'aggregateDogFactEntity',
    );
    return AggregateDogFactEntity(query);
  }

  Future<Iterable<DogFactEntityGroupByOutputType>> groupBy({
    DogFactEntityWhereInput? where,
    Iterable<DogFactEntityOrderByWithAggregationInput>? orderBy,
    required Iterable<DogFactEntityScalarFieldEnum> by,
    DogFactEntityScalarWhereWithAggregatesInput? having,
    int? take,
    int? skip,
  }) {
    final args = [
      _i2.GraphQLArg(
        r'where',
        where,
      ),
      _i2.GraphQLArg(
        r'orderBy',
        orderBy,
      ),
      _i2.GraphQLArg(
        r'by',
        by,
      ),
      _i2.GraphQLArg(
        r'having',
        having,
      ),
      _i2.GraphQLArg(
        r'take',
        take,
      ),
      _i2.GraphQLArg(
        r'skip',
        skip,
      ),
    ];
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'groupByDogFactEntity',
          fields: fields,
          args: args,
        )
      ]),
      key: r'groupByDogFactEntity',
    );
    final fields = by.map((e) => _i2.GraphQLField(e.originalName ?? e.name));
    compiler(Iterable<Map> groupByDogFactEntity) => groupByDogFactEntity.map(
        (Map groupByDogFactEntity) => DogFactEntityGroupByOutputType.fromJson(
            groupByDogFactEntity.cast()));
    return query(fields).then((json) => json is Iterable
        ? compiler(json.cast())
        : throw Exception('Unable to parse response'));
  }
}

@_i1.jsonSerializable
class DogFactEntityGroupByOutputType implements _i1.JsonSerializable {
  const DogFactEntityGroupByOutputType({
    this.id,
    this.factId,
    this.fact,
  });

  factory DogFactEntityGroupByOutputType.fromJson(Map<String, dynamic> json) =>
      _$DogFactEntityGroupByOutputTypeFromJson(json);

  final int? id;

  final String? factId;

  final String? fact;

  @override
  Map<String, dynamic> toJson() => _$DogFactEntityGroupByOutputTypeToJson(this);
}

@_i1.jsonSerializable
class AffectedRowsOutput implements _i1.JsonSerializable {
  const AffectedRowsOutput({this.count});

  factory AffectedRowsOutput.fromJson(Map<String, dynamic> json) =>
      _$AffectedRowsOutputFromJson(json);

  final int? count;

  @override
  Map<String, dynamic> toJson() => _$AffectedRowsOutputToJson(this);
}

class AggregateDogFactEntity {
  const AggregateDogFactEntity(this.$query);

  final _i1.PrismaFluentQuery $query;

  DogFactEntityCountAggregateOutputType $count() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'_count',
          fields: fields,
        )
      ]),
      key: r'_count',
    );
    return DogFactEntityCountAggregateOutputType(query);
  }

  DogFactEntityAvgAggregateOutputType $avg() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'_avg',
          fields: fields,
        )
      ]),
      key: r'_avg',
    );
    return DogFactEntityAvgAggregateOutputType(query);
  }

  DogFactEntitySumAggregateOutputType $sum() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'_sum',
          fields: fields,
        )
      ]),
      key: r'_sum',
    );
    return DogFactEntitySumAggregateOutputType(query);
  }

  DogFactEntityMinAggregateOutputType $min() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'_min',
          fields: fields,
        )
      ]),
      key: r'_min',
    );
    return DogFactEntityMinAggregateOutputType(query);
  }

  DogFactEntityMaxAggregateOutputType $max() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'_max',
          fields: fields,
        )
      ]),
      key: r'_max',
    );
    return DogFactEntityMaxAggregateOutputType(query);
  }
}

class DogFactEntityCountAggregateOutputType {
  const DogFactEntityCountAggregateOutputType(this.$query);

  final _i1.PrismaFluentQuery $query;

  Future<int> id() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'id',
          fields: fields,
        )
      ]),
      key: r'id',
    );
    return query(const []).then((value) => (value as int));
  }

  Future<int> factId() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'factId',
          fields: fields,
        )
      ]),
      key: r'factId',
    );
    return query(const []).then((value) => (value as int));
  }

  Future<int> fact() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'fact',
          fields: fields,
        )
      ]),
      key: r'fact',
    );
    return query(const []).then((value) => (value as int));
  }

  Future<int> $all() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'_all',
          fields: fields,
        )
      ]),
      key: r'_all',
    );
    return query(const []).then((value) => (value as int));
  }
}

class DogFactEntityAvgAggregateOutputType {
  const DogFactEntityAvgAggregateOutputType(this.$query);

  final _i1.PrismaFluentQuery $query;

  Future<double?> id() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'id',
          fields: fields,
        )
      ]),
      key: r'id',
    );
    return query(const []).then((value) => (value as double?));
  }
}

class DogFactEntitySumAggregateOutputType {
  const DogFactEntitySumAggregateOutputType(this.$query);

  final _i1.PrismaFluentQuery $query;

  Future<int?> id() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'id',
          fields: fields,
        )
      ]),
      key: r'id',
    );
    return query(const []).then((value) => (value as int?));
  }
}

class DogFactEntityMinAggregateOutputType {
  const DogFactEntityMinAggregateOutputType(this.$query);

  final _i1.PrismaFluentQuery $query;

  Future<int?> id() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'id',
          fields: fields,
        )
      ]),
      key: r'id',
    );
    return query(const []).then((value) => (value as int?));
  }

  Future<String?> factId() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'factId',
          fields: fields,
        )
      ]),
      key: r'factId',
    );
    return query(const []).then((value) => (value as String?));
  }

  Future<String?> fact() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'fact',
          fields: fields,
        )
      ]),
      key: r'fact',
    );
    return query(const []).then((value) => (value as String?));
  }
}

class DogFactEntityMaxAggregateOutputType {
  const DogFactEntityMaxAggregateOutputType(this.$query);

  final _i1.PrismaFluentQuery $query;

  Future<int?> id() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'id',
          fields: fields,
        )
      ]),
      key: r'id',
    );
    return query(const []).then((value) => (value as int?));
  }

  Future<String?> factId() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'factId',
          fields: fields,
        )
      ]),
      key: r'factId',
    );
    return query(const []).then((value) => (value as String?));
  }

  Future<String?> fact() {
    final query = _i1.PrismaFluent.queryBuilder(
      query: (fields) => $query([
        _i2.GraphQLField(
          r'fact',
          fields: fields,
        )
      ]),
      key: r'fact',
    );
    return query(const []).then((value) => (value as String?));
  }
}

@JsonSerializable(
  createFactory: false,
  createToJson: true,
  includeIfNull: false,
)
class Datasources implements _i1.JsonSerializable {
  const Datasources({this.db});

  final String? db;

  @override
  Map<String, dynamic> toJson() => _$DatasourcesToJson(this);
}

class PrismaClient extends _i1.BasePrismaClient<PrismaClient> {
  PrismaClient._internal(
    _i3.Engine engine, {
    _i3.QueryEngineRequestHeaders? headers,
    _i3.TransactionInfo? transaction,
  })  : _engine = engine,
        _headers = headers,
        _transaction = transaction,
        super(
          engine,
          headers: headers,
          transaction: transaction,
        );

  factory PrismaClient({
    Datasources? datasources,
    Iterable<_i4.Event>? stdout,
    Iterable<_i4.Event>? event,
  }) {
    final logger = _i4.Logger(
      stdout: stdout,
      event: event,
    );
    final engine = _i5.BinaryEngine(
      logger: logger,
      schema:
          r'Ly8gVGhpcyBpcyB5b3VyIFByaXNtYSBzY2hlbWEgZmlsZSwKLy8gbGVhcm4gbW9yZSBhYm91dCBpdCBpbiB0aGUgZG9jczogaHR0cHM6Ly9wcmlzLmx5L2QvcHJpc21hLXNjaGVtYQoKZ2VuZXJhdG9yIGNsaWVudCB7CiAgcHJvdmlkZXIgPSAiZGFydCBydW4gb3JtIgp9CgpkYXRhc291cmNlIGRiIHsKICBwcm92aWRlciA9ICJwb3N0Z3Jlc3FsIgogIHVybCAgICAgID0gZW52KCJEQVRBQkFTRV9VUkwiKQp9Cgptb2RlbCBEb2dGYWN0RW50aXR5IHsKICAgaWQgSW50IEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpCiAgIGZhY3RJZCBTdHJpbmcgQHVuaXF1ZQogICBmYWN0IFN0cmluZwp9Cg==',
      datasources: datasources?.toJson().cast() ?? const {},
      executable:
          r'/Users/neto/src/dart/dart-demo/node_modules/prisma/query-engine-darwin-arm64',
    );
    return PrismaClient._internal(engine);
  }

  final _i3.Engine _engine;

  final _i3.QueryEngineRequestHeaders? _headers;

  final _i3.TransactionInfo? _transaction;

  @override
  PrismaClient copyWith({
    _i3.QueryEngineRequestHeaders? headers,
    _i3.TransactionInfo? transaction,
  }) =>
      PrismaClient._internal(
        _engine,
        headers: headers ?? _headers,
        transaction: transaction ?? _transaction,
      );

  _i1.ModelDelegate<DogFactEntity> get dogFactEntity =>
      _i1.ModelDelegate<DogFactEntity>(
        _engine,
        headers: _headers,
        transaction: _transaction,
      );
}
