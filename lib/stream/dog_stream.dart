import '../client/dto.dart';
import '../types.dart';
import 'package:http/http.dart' as http;

Stream<DogFactDto> stream(FACT fact) async* {
  while (true) {
    yield await fact(http.get);
  }
}
