import 'dart:io';

import 'package:dart_demo/client/dog_client.dart';
import 'package:dart_demo/db/db.dart';
import 'package:dart_demo/db/dog_fact.dart';
import 'package:dart_demo/src/generated/prisma/prisma_client.dart';
import 'package:dart_demo/stream/dog_stream.dart';

main(List<String> arguments) async {
  var streaming = stream(fact);
  await for (final dogFactDto in streaming) {
    print('saving... ${dogFactDto.data[0].attributes.body}');
    save(dogFactDto, postgres.dogFactEntity.create);
    print('saved... ${dogFactDto.data[0].id}');
    sleep(Duration(seconds: 10));
  }
}