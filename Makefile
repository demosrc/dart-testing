unit-test:
	dart run test

demo:
	dart run bin/dart_demo.dart

dbdeploy:
	docker-compose up

dbpush:
	npx prisma db push

gprisma:
	npx prisma generate

buildrunner:
	dart run build_runner build