import 'dart:async';
import 'dart:convert';

import 'package:dart_demo/client/dto.dart';
import 'package:dart_demo/src/generated/prisma/prisma_client.dart';
import 'package:dart_demo/types.dart';
import 'package:http/http.dart';
import 'package:orm/graphql.dart';

final dogFactDto = DogFactDto(data: [
  FactData(
      id: '239e3b29-d812-467a-addd-b82ad0385819',
      type: 'fact',
      attributes: FactAttribute(
          body: 'Dogs can see in color, but not as vividly as humans.'))
]);

GET fakeGet(dynamic body) {
  return (Uri url, {Map<String, String>? headers}) {
    var completer = new Completer<Response>();
    completer.complete(Response.bytes(utf8.encode(jsonEncode(body)), 200));
    return completer.future;
  };
}

FACT fakeFact(dynamic body) {
  return (GET get) {
    var completer = new Completer<DogFactDto>();
    completer.complete(body);
    return completer.future;
  };
}


SAVE fakeFunctionSave(dynamic body) {
  return ({required DogFactEntityCreateInput data}) {
    var completer = new Completer<DogFactEntity>();
    completer.complete(body);
    return new DogFactEntityFluent<DogFactEntity>(completer.future, fakeQuery(body));
  };
}

QUERY fakeQuery(dynamic body) {
  return (Iterable<GraphQLField> i) {
    var completer = new Completer<DogFactDto>();
    completer.complete(body);
    return completer.future;
  };
}