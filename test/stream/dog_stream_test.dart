import 'package:dart_demo/client/dto.dart';
import 'package:dart_demo/stream/dog_stream.dart';
import 'package:test/scaffolding.dart';

import '../util.dart';

void main() {
  test('should use function as parameter to fetch data and stream it',
      () async {
    stream(fakeFact(dogFactDto))
        .elementAt(0)
        .asStream()
        .forEach((streamedFact) {
      assert(streamedFact.toString().compareTo(dogFactDto.toString()) == 0,
          'retrieved fact does not match with expected');
    });
  });
}
