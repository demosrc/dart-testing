import 'package:dart_demo/client/dog_client.dart';
import 'package:test/scaffolding.dart';

import '../../lib/client/dto.dart';
import '../util.dart';

void main() {
  test('should use function as parameter to fetch data', () async {
    var retrievedDogFact = await fact(fakeGet(dogFactDto));

    assert(retrievedDogFact.toString().compareTo(dogFactDto.toString()) == 0,
        'retrieved fact does not match with expected');
  });
}
