import 'package:dart_demo/db/dog_fact.dart';
import 'package:dart_demo/src/generated/prisma/prisma_client.dart';
import 'package:test/scaffolding.dart';

import '../util.dart';

void main() {
  var dogFactModel = DogFactEntity(
      id: 0,
      factId: '239e3b29-d812-467a-addd-b82ad0385819',
      fact: 'Dogs can see in color, but not as vividly as humans.');

  test(
      'should use function as parameter to save and return dog fact in database',
      () async {
    var retrievedDogFact =
        await save(dogFactDto, fakeFunctionSave(dogFactModel));
    print(retrievedDogFact.toString());

    assert(retrievedDogFact.id == 0,
        'saved dog fact id does not match with expected');
    assert(retrievedDogFact.factId == dogFactModel.factId,
        'saved dog fact factId does not match with expected');
    assert(retrievedDogFact.fact == dogFactModel.fact,
        'saved fact does not match with expected');
  });
}
